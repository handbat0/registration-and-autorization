<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Добро пожаловать!</title>
  </head>
  <body>
      
      
    <content>
       <div class="container main">
           <div class="row">
               <div class="col"></div>
               <div class="col-10 col-md-6">
                    <form class="clearfix" id="form-aut" action="">
                      <div class="form-group">
                        <label for="login">Логин</label>
                        <input type="login" class="form-control" name="login" id="login" placeholder="Введите логин" alt="123">
                      </div>
                      <div class="form-group">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Введите пароль">   
                      </div>
                       
                          <a href="registration.php" class="btn btn-outline-primary btn-lg btn-block" role="button" aria-pressed="true">Зарегистрироваться</a>
                        
                          <input type="text" class="form-control d-none" name="type" value="auth">
                          <button type="submit" class="btn btn-outline-success btn-lg btn-block mt-2" id="btn-aut" aria-pressed="true">Войти</button>
                    </form>
               </div>
               <div class="col"></div>
            </div>
       </div>  
    </content> 
          
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
  </body>
</html>