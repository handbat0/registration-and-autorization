//Registration
$( document ).ready(function() {
    $("#btn-reg").click(
		function(){
			sendAjaxForm('form-reg');
			return false; 
		}
	);
});

//Autoriztion
$( document ).ready(function() {
    $("#btn-aut").click(
		function(){
			sendAjaxForm('form-aut');
			return false; 
		}
	);
});

//Exit (unset session)
$( document ).ready(function() {
    $("#btn-exit").click(
		function(){
			sendAjaxForm('');
			return false; 
		}
	);
});
 
function sendAjaxForm(form) {
    jQuery.ajax({
        url:     'api/api.php', //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "json", //формат данных
        data: jQuery("#"+form).serialize(),  // Сеарилизуем объект
        success: function(data) { //Данные отправлены успешно
          if(data.status == 'success'){
                location.href = "http://handbat0/index.php";
            }else if(data.status == 'error'){
                alert(data.text);
            }
    	},
    	error: function(data) { // Данные не отправлены
            alert('Ошибка! Данные не отправлены!');
            console.log(data);
    	}
 	});
}