<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Добро пожаловать, <?php echo $_SESSION['login']; ?>!</title>
  </head>
  <body>
    
    <content>
       <div class="container main">
           
        <div class="row align-items-center">
            <div class="col"></div>
            <div class="col-12 col-sm-8 col-md-7 col-lg-7 text-center user">
                Здравствуйте, уважаемый <?php echo $_SESSION['surname']." ".$_SESSION['name'];  ?>!
            </div>
            <div class="col"></div>
        </div>

           
        <div class="row align-items-center mt-2">
            <div class="col"></div>
            <div class="col-12 col-sm-8 col-md-7 col-lg-7 text-center">
                <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    История кликов
                </button>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body text-left">
                        Клики!<br>
                        Rkbrb!
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
           
        <div class="row align-items-center mt-2">
            <div class="col"></div>
            <div class="col-12 col-sm-8 col-md-5 col-lg-3 text-center">
                <a href="authorization.php" id="btn-exit" class="btn btn-outline-dark btn-lg btn-block" role="button" aria-pressed="true">Выйти</a>
            </div>
            <div class="col"></div>
        </div>
       </div>  
    </content> 
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
  </body>
</html>