<?PHP    
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $login = clean($_POST['login']);
    $pwd = clean($_POST['password']); 
    $email = clean($_POST['email']);
    $surname = clean($_POST['surname']);
    $name = clean($_POST['name']);
    $patronymic = clean($_POST['patronymic']);
    if($patronymic===null){
        $patronymic='';
    }
    $sex = clean($_POST['sex']);

    switch ($_POST['type']) {
        case 'auth':
            if(validAuth($login, $pwd)){
                require_once('EnterUser.php');
            }
            break;
        case 'reg':
            if(validReg($login, $pwd, $email, $surname, $name, $patronymic, $sex)){
                require_once('AddUser.php');
            }
            break;
        default:
            closeSession();
    }
}

function validReg($login, $pwd, $email, $surname, $name, $patronymic, $sex){
    
    if(empty($login) || empty($pwd) || empty($email) || empty($surname) || empty($name) || empty($sex)){
        message("error", "Ошибка! Одно из полей не заполнено.");
        exit;
    }
    
    if(!check_length($login,5,15) || !check_length($pwd,5,15)) {  
        message("error", "Ошибка! Логин и пароль должен быть от 5 до 15 символов.");
        exit;
    }   

    if(!check_length($surname,2,15) || !check_length($name,2,15)) {
        message("error", "Ошибка! Фамилия и Имя должны быть от 2 до 15 символов.");
        exit;
    }

    if(!check_length($patronymic,0,30)) {
        message("error", "Ошибка! Отчетсво не может быть больше 30 символов.");
        exit;
    }
    
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        message("error", "Ошибка! Введён некорректный E-mail.");
        exit;
    }
    
    return true;
}

function validAuth($login, $pwd){

    if(empty($login) || empty($pwd)){
        message("error", "Ошибка! Одно из полей не заполнено.");
        exit;
    }

    if(!check_length($login,5,15) || !check_length($pwd,5,15)){
        message("error", "Ошибка! Логин и пароль должен быть от 5 до 15 символов.");
        exit;
    }
    return true;
}

//Отчищаем переменную от специальных символов и лишних пробелов
function clean($value = "") {
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    return $value;
}

//Проверяем длину переменной
function check_length($value = "", $min, $max) {
    $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);
    return !$result;
}

//Выдаём сообщение
function message($status, $text) {
    $msg = array(
        "status" => $status,
        "text" => $text
    );
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($msg);
}

//Создание сессии
function startSession($userLogin, $userName, $userSurname){
    session_start();
    $_SESSION['login'] = $userLogin;
    $_SESSION['name'] = $userName;
    $_SESSION['surname'] = $userSurname;
}

function closeSession(){
    session_start();
    session_unset($_SESSION['login']);
    session_destroy();
    message("success", "");
}
?>